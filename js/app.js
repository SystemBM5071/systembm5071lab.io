        // 博客文章数据
        var posts = [
        {
                title: "人生中的第一篇博文",
                content: "这是我的第一篇博客文章。十分感谢国内ChatGPT的镜像网站，帮助我生成了这个博客网站的代码。在这个代码内，它运用了HTML、CSS语言与JavaSrcipt语言，组合成为了这个网站！也十分感谢GitLab Pages给予的网页部署服务！如果想知道要怎么部署的话，请前去哔哩哔哩搜索SystemBM5071，并关注我，我会及时更新哒！"
            },
            {
                title: "未完待续。。。",
                content: "未完待续。。。"
            },
        ];

        // 生成博客文章列表
        var blogPostsContainer = document.getElementById("blogPosts");
        posts.forEach(function(post) {
            var postElement = document.createElement("div");
            postElement.classList.add("blogPost");

            var titleElement = document.createElement("h2");
            titleElement.textContent = post.title;

            var contentElement = document.createElement("p");
            contentElement.textContent = post.content;

            postElement.appendChild(titleElement);
            postElement.appendChild(contentElement);

            blogPostsContainer.appendChild(postElement);
        });